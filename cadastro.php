<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
Author  : Leandro
Date    : 08/06/2015
-->

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>HotSpot - Cadastro</title>
        
        <link href="estilo.css" media="screen" rel="stylesheet" type="text/css" />
        <script src="js/jquery.js"></script>
        <script src="js/jquery.mask.js"></script>
        <script src="js/validateCPF.js"></script>
        <script type="text/javascript">
            $(function() {
                $("#cpf").mask("999.999.999-99");
                $("#telefone").mask("(99) 99999-9999");
                
                $("#cpf").blur(function (){
                    if(!validarCPF($("#cpf").val())) {
                        if($("#cpf").val() !== '') {
                            alert("CPF inv�lido.");
                            $("#cpf").val('');
                            $("#cpf").focus();
                        }
                    }
                })
            });
        </script>
    </head>
    <body>
        <div id="geral">
            <div id="topo">
                <img width="250" height="250" src="imagens/wifi.png" alt="wifi-logo" title="Wifi" />
            </div>
            
            <form name="formCadastro" action="postCadastro.php" method="post" onsubmit="checkTermo()">
            <div id="form">
                <ul>
                    <li>
                        <label>Nome:</label>
                        <input type="text" name="nome" placeholder="Nome completo" value="" required />
                    </li>
                    <li>
                        <label>Data de nascimento</label>
                        <input type="date" name="nascimento" value="" required />
                    </li>
                    <li>
                        <label>Telefone:</label>
                        <input type="text" id="telefone" name="telefone" placeholder="ex: (21) 99999-9999" maxlength="15" value="" required />
                    </li>
                    <li>
                        <label>Email:</label>
                        <input type="email" name="email" placeholder="ex: seuemail@servidor.com" value="" required />
                    </li>
                    <li>
                        <label>CPF:</label>
                        <input type="text" id="cpf" name="cpf" placeholder="999.999.999-99" maxlength="14" value="" required />
                    </li>
                    <li>
                        <label>Senha:</label>
                        <input type="password" name="pass" id="pass" placeholder="*****" value="" required />
                    </li>
                    <li>
                        <label>Confirmar senha:</label>
                        <input type="password" name="conf_pass" id="conf_pass" placeholder="*****" value="" onblur="validatePass()" required />
                    </li>
                    <li>
                        <label><input type="checkbox" name="termo" id="termo" value="1" required /> <b>Li e concordo com os termos de uso.</b></label>
                    </li>
                    <li style="text-align: center">
                        <input type="submit" value="Cadastrar" />
                    </li>
                </ul>
            </div>
            </form>
            
        </div>
        <div id="rodape">
            <img src="imagens/logo.jpg" alt="giga-logo" title="Gigacom" />
            <br />
            <a href="mailto:leandroomagalhaes@gmail.com">Desenvolvido por: Leandro Magalh�es</a>
        </div>
    </body>
</html>

<script type="text/javascript">
    function validatePass() {
        if(document.getElementById("pass").value !== document.getElementById("conf_pass").value){
            alert("Confirma��o da senha n�o confere!");
            document.getElementById("conf_pass").value = null;
            document.getElementById("pass").value = null;
            document.getElementById("pass").focus();
        }
    }
    
    function checkTermo() {
        if(document.getElementById("termo").value !== "1")
            return false;
        
        alert('Cadastro efetuado com sucesso!');
        return true;
    }
    
</script>
