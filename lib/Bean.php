<?php

require_once 'rb.php';

class Bean {
    
    function __construct() {
        R::setup("mysql:host=localhost;dbname=mklogin", "root", "");
        R::addDatabase("radius", "mysql:host=10.99.1.117;dbname=radius", "radadmin", "gc.H0tSp0t");
    }

    private function setRadius(){
        R::selectDatabase("radius");
    }
    
    private function setDB() {
        R::selectDatabase("default");
    }
    
    private function saveUserRadius($username, $pass){
        $this->setRadius();
        
        #INSERT INTO radius.radcheck (`username`,`attribute`,`op`,`value`) VALUES ("<login>","Cleartext-Password",":=","<pass>");
        $user = R::dispense("radcheck");
        $user->username     = $username;
        $user->value        = $pass;
        $user->attribute    = "Cleartext-Password";
        $user->op           = ":=";
        R::store($user);
    }
    
    private function deleteUserRadius($username) {
        $this->setRadius();
        $user = R::find("radcheck", " username = ? ", array($username));
        R::trashAll($user);
    }
    
    public function saveCliente($dados){
        $this->setDB();
        
        $cliente = R::dispense("cliente");
        $cliente->nome          = $dados["nome"];
        $cliente->nascimento    = $dados["nascimento"];
        $cliente->telefone      = $dados["telefone"];
        $cliente->email         = $dados["email"];
        $cliente->cpf           = $dados["cpf"];
        $cliente->senha         = $dados["pass"];
        $cliente->criado_em     = R::isoDateTime();
        R::store($cliente);
        
        $this->saveUserRadius($cliente->cpf, $cliente->senha);
    }
    
    public function deleteCliente($id) {
        $this->setDB();
        
        $cliente = R::load("cliente", $id);
        R::trash($cliente);
        
        $this->deleteUserRadius($cliente->cpf);
    }
    
    public function listAllCliente() {
        return R::findAll("cliente");
    }
}

