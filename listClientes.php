<?php

require_once 'lib/Bean.php';

$bean = new Bean();
$clientes = $bean->listAllCliente();
?>

<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
Author  : Leandro
Date    : 08/06/2015
-->

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>HotSpot - Clientes</title>
        
        <link href="estilo.css" media="screen" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    </head>
    <body>
        <div id="geral" style="width: 650px;">
            <div id="topo">
                <img width="250" height="250" src="imagens/wifi.png" alt="wifi-logo" title="Wifi" />
            </div>
            
            <table class="grid" cellspacing="0" cellpadding="0">
                <thead>
                    <th>Nome</th>
                    <th>Login</th>
                    <th>Criado em</th>
                    <th>A��es</th>
                </thead>
                <tbody>
                    <?php
                    foreach ($clientes as $cliente) {
                        echo "<tr>";
                        echo "<td>$cliente->nome</td>";
                        echo "<td>$cliente->login</td>";
                        echo "<td>$cliente->criado_em</td>";
                        echo "<td><a href='excluirCliente.php?id=$cliente->id'><i class='fa fa-trash-o'>Excluir</i></a></td>";
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
            
        </div>
        <div id="rodape">
            <img src="imagens/logo.jpg" alt="giga-logo" title="Gigacom" />
            <br />
            <a href="mailto:leandroomagalhaes@gmail.com">Desenvolvido por: Leandro Magalh�es</a>
        </div>
    </body>
</html>

<script type="text/javascript">
    function validatePass() {
        if(document.getElementById("pass").value !== document.getElementById("conf_pass").value){
            alert("Confirma��o da senha n�o confere!");
            document.getElementById("conf_pass").value = null;
            document.getElementById("pass").value = null;
            document.getElementById("pass").focus();
        }
    }
    
    function checkTermo() {
        if(document.getElementById("termo").value !== "1")
            return false;
        
        return true;
    }
</script>
